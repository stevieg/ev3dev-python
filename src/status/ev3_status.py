#!/usr/bin/env python

import ev3dev.auto as ev3
import pyudev

ev3_sysfs_class = ['lego-sensor','lego-port','power_supply','servo-motor','tacho-motor','leds']

# Initialise context for use in examining devices
ctx = pyudev.Context()

# Enumerate the ports, covering both input and output
ports = dict()
for d in ctx.list_devices(subsystem='lego-port'):
    port_data = dict()
    for (k,v) in d.items():
        port_data[k] = v
    ports[port_data['LEGO_ADDRESS']] = port_data

# Enumerate the sensors, and attach to ports 
sensors = dict()
for d in ctx.list_devices(subsystem='lego-sensor'):
    sensor_data = dict()
    for (k,v) in d.items():
        sensor_data[k] = v
    if sensor_data['LEGO_DRIVER_NAME'] not in sensors:
        sensors[sensor_data['LEGO_DRIVER_NAME']] = list()
    sensors[sensor_data['LEGO_DRIVER_NAME']].append(sensor_data)
    ports[sensor_data['LEGO_ADDRESS']]['attached'] = sensor_data

# Enumerate the motors, covering both tacho and servo types
motors = dict()
for d in ctx.list_devices(subsystem='tacho-motor'):
    motor_data = dict()
    for (k,v) in d.items():
        motor_data[k] = v
    if motor_data['LEGO_DRIVER_NAME'] not in motors:
        motors[motor_data['LEGO_DRIVER_NAME']] = list()
    motors[motor_data['LEGO_DRIVER_NAME']].append(motor_data)
    ports[motor_data['LEGO_ADDRESS']]['attached'] = motor_data
for d in ctx.list_devices(subsystem='servoo-motor'):
    motor_data = dict()
    for (k,v) in d.items():
        motor_data[k] = v
    if motor_data['LEGO_DRIVER_NAME'] not in motors:
        motors[motor_data['LEGO_DRIVER_NAME']] = list()
    motors[motor_data['LEGO_DRIVER_NAME']].append(motor_data)
    ports[motor_data['LEGO_ADDRESS']]['attached'] = motor_data

# Now iterate through attached devices and instantiate objects
for dev_type in sensors:
    for dev in sensors[dev_type]:
        obj = generate_ev3_object(dev)
        ports[dev['LEGO_ADDRESS']]['object'] = obj
# Repeat for motors?
# Repeat for LEDs?
# Repeat for PowerSupply?
# Repeat for Sound?

#ev3.LargeMotor('outB')


def generate_ev3_object(dev):
    # Need to map from driver-name to class
    cls = driver_to_class.get(dev['LEGO_DRIVER_NAME'],None)
